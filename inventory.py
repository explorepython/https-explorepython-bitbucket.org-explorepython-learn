import tkinter as tk
from tkinter import ttk
import json


class Inventory:
    def __init__(self, win):

        # Label 1
        self.name_label = ttk.Label(win, text='Choose the brand')
        self.name_label.grid(row=0, column=0, sticky=tk.W)
        # Label 2
        self.volume_label = ttk.Label(win, text='Enter the volume')
        self.volume_label.grid(row=2, column=0, sticky=tk.W)
        # Entry box1
        self.brand_var1 = tk.StringVar()
        self.brand_entry1 = ttk.Entry(win, width=16, textvariable=self.brand_var1)
        self.brand_entry1.grid(row=2, column=1)

        # drop down option
        self.brand_option = tk.StringVar()
        self.brand_combo = ttk.Combobox(win, width=14, textvariable=self.brand_option, state='readonly')
        self.brand_combo['values'] = ('SELECT', 'JD', 'TEACHERS', 'VAT')
        self.brand_combo.current(0)
        self.brand_combo.grid(row=0, column=1)

        # Submit button
        self.submit_button = ttk.Button(win, text='Submit', command=self.action)
        self.submit_button.grid(row=4, column=0)

    def action(self):
        brand_name = self.brand_var1.get()
        volume_num = self.brand_option.get()
        file = open("file.json", "r")
        dictionary = json.load(file)
        for key in dictionary.keys():
            if key == volume_num:
                new_value = int(dictionary[key]) - int(brand_name)
                final_value = str(new_value)
                dictionary.update({volume_num: int(final_value)})
        json_object = json.dumps(dictionary, indent=4)
        with open("file.json", "w") as outfile:
            outfile.write(json_object)
        self.brand_entry1.destroy()
        self.brand_combo.destroy()
        self.name_label.destroy()
        self.volume_label.destroy()
        self.submit_button.destroy()
        abc = "{} {} {}".format(*dictionary.items())
        name_label2 = ttk.Label(win, text=abc)
        name_label2.grid(row=0, column=0, sticky=tk.W)


# main frame
win = tk.Tk()
win.geometry("400x150")
mywin = Inventory(win)
win.title('SELF MANAGEMENT')

win.mainloop()

