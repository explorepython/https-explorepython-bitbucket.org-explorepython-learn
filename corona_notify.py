import time
from urllib import request
from plyer import notification
import requests
from bs4 import BeautifulSoup

# Notification function
def notify(title, message):
    notification.notify(
        title=title,
        message=message,
        app_icon=None,
        timeout=6
    )


if __name__ == "__main__":
    newValue = requests.get('https://www.mohfw.gov.in/').text
    soup = BeautifulSoup(newValue, 'html.parser')
    myData = ""
    for tr in soup.find_all('tr'):
        myData = myData + tr.get_text()
        myDatalist = myData.split("\n\n")
    states = ['Bihar', 'Delhi']
    for newData in myDatalist[1:31]:
        dataBag = newData.split('\n')
        if dataBag[1] in states:
            title = 'Coronacase'
            textdata = f" States: {dataBag[1]}\n Total:{dataBag[2]} \ncured: {dataBag[3]} \ndeath: {dataBag[4]}"
            notify(title, textdata)



